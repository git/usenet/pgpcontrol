#! /usr/bin/perl -w
$ID = q$ID$;
#
# sign-newsart -- Simple wrapper to sign a control message with News::Article.
#
# Written by Russ Allbery <eagle@eyrie.org>
# This work is hereby placed in the public domain by its author.
#
# This program is used to test that pgpverify can correctly verify signatures
# made by News::Article.  It generates two signed messages, signed.pgp and
# signed.gpg, in the current directory from the file given on the command
# line.

use strict;
use vars qw($ID);

use News::Article;
use PGP::Sign ();

# Find a program on the user's path.
sub find_program {
    my ($program) = @_;
    my @path = split (/:/, $ENV{PATH});
    for (@path) {
        return "$_/$program" if -x "$_/$program";
    }
    return $program;
}

my $article = News::Article->new ($ARGV[0]);
open (PASS, 'keyring/passphrase')
    or die "Can't open keyring/passphrase: $!\n";
my $passphrase = <PASS>;
chomp $passphrase;
close PASS;
$PGP::Sign::PGPPATH = 'keyring';
$PGP::Sign::PGPSTYLE = 'PGP2';
$PGP::Sign::PGPS = find_program ('pgp');
$PGP::Sign::PGPV = $PGP::Sign::PGPS;
my $error = $article->sign_control ('testing', $passphrase);
die "$error\n" if $error;
open (PGP, '> signed.pgp') or die "Can't create signed.pgp: $!\n";
$article->write (\*PGP);
close PGP;
$PGP::Sign::PGPSTYLE = 'GPG';
$PGP::Sign::PGPS = find_program ('gpg');
$PGP::Sign::PGPV = find_program ('gpgv');
$error = $article->sign_control ('testing', $passphrase);
die "$error\n" if $error;
open (PGP, '> signed.gpg') or die "Can't create signed.gpg: $!\n";
$article->write (\*PGP);
close PGP;
exit 0;
