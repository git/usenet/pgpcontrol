#!/bin/sh
# vi:ts=4:ai
# $Id$
#
# Unterzeichnet Newsartikel mittels PGP
# Heiko Schlichting (heiko@fu-berlin.de)
#
# $Log$
# Revision 1.2  2011/08/31 20:16:07  eagle
# Filter out Xref header and sign Supersedes
#
# Revision 1.10  1996/09/15 02:19:04  heiko
# Also sign Supersedes
#
# Revision 1.9  1996/08/09 23:31:43  heiko
# Filtering Xref header
#
# Revision 1.8  1996/08/09 22:25:27  heiko
# ignore case in headers but keep the right case to sign
# filter unwanted headerlines
# sign an already signed article works now
#
# Revision 1.7  1996/08/07 19:00:08  heiko
# SIGNER could contain spaces.
#
# Revision 1.6  1996/08/07  18:17:13  lutz
# SIGNER might be preset. (lutz@as-node.jena.thur.de)
#
# Revision 1.5  1996/08/07  01:36:35  heiko
# Skip articles if not found.
#
# Revision 1.4  1996/08/07  01:15:38  heiko
# Debug statement removed
#
# Revision 1.3  1996/08/07  01:14:22  heiko
# Initial Version
#
#

###################### START OF CONFIGURATION SECTION ######################

# Configure pathnames to required programs
GAWK=/usr/local/bin/gawk
PGP=/usr/local/bin/pgp

# Who should sign the article?
if [ -z "$SIGNER" ]; then
	SIGNER="de.admin.news.announce"
fi


# STORING YOUR PASS PHRASE IN A FILE IS A SECURITY HOLE!
# If you don't define PGPPASSFILE, you can use this script interactively.
PGPPASSFILE=""

####################### END OF CONFIGURATION SECTION #######################

PRG=$0

check()
{
	if [ ! -x $1 ]; then
		echo "Required executable $1 not found."
		echo "You should configure $PRG."
		exit 2
	fi
}

# Usage message
if [ $# -eq 0 ]; then
	echo "Usage: $PRG article ..."
	exit 1
fi

# Are the executables available?
for EXE in $GAWK $PGP;
do
	check "$EXE"
done

if [ ! -z "$PGPPASSFILE" -a -f "$PGPPASSFILE" ];
then
	PGPPASS=`cat "$PGPPASSFILE"`
	export PGPPASS
fi

while [ $# -gt 0 ]; do
	article=$1
	if [ ! -f ${article} ]; then
		echo "${article}: not found - skipped"
		shift
		continue
	fi
	rm -f ${article}.new
	trap "rm -f ${article}.new; exit 0" 0 1 2 3 15
	$GAWK -F: '
		BEGIN{
			# Which headers should be signed?
			headernames="Subject:Control:Message-ID:Date:From:Sender:Newsgroups:Approved:Followup-To:Supersedes"
			split(headernames,header)
			IGNORECASE=1
			body=0
			i=0
			last=0
		}

		# After processing the header, print it
		!NF&&!body{
			body=1
			print "X-Signed-Headers:",sig
			for (h=0; h < i; h++) {
				print ph[h]
			}
		}

		# Article body
		body{print;next}

		# For continuation header lines
		$0~/^[ \t]/ {
			if (last==1) {
				ph[i-1]=ph[i-1]"\n"$0
			}
			next
		}

		# Normal header lines
		{
			for (h in header) {
				if (match($1,"^"header[h]"$")){
					ph[i]=$0
					if (i) {
						sig=sig","$1
					}else{
						sig=$1
					}
					i++
					last=1
					next
				}else{
					last=0
				}
			}
		}
	' $article | $PGP -fast -u "$SIGNER" +verbose=0 +language="en" | 
	$GAWK -v artfile="$article" '
		BEGIN{
			body=0
			skip=0
			version="unknown"
			# Which header should be filtered?
			filternames="X-PGP-Sig:NNTP-Posting-Host:NNTP-Posting-User:Path:To:Cc:X-Access:Lines:X-Newsreader:Xref"
			split(filternames,filter,":")
			IGNORECASE=1
			while ((getline header < artfile) >0) {
				if (length(header)>0) {
					if (skip && match(header,/^[ \t]/)){
						continue
					}
					skip=0
					for (f in filter) {
						if (match(header,"^"filter[f]":[ \t]")){
							skip=1
						}
					}
					if (!skip) {
						print header
					}
				} else {
				break
				}
			}
			IGNORECASE=0
		}
		$1~/^X-Signed-Headers:/&&!body{
			sig=$2
			next
		}
		$0~/^-----BEGIN PGP SIGNATURE-----$/{
			body=1
			next
		}
		$0~/^Version:/&&(body==1){
			version=$2
			next
		}
		$0~/^-----END PGP SIGNATURE-----$/{
			body=0
			next
		}
		!NF&&(body==1){
			body=2
			print "X-PGP-Sig:",version,sig
			next
		}
		body==2{
			print "\t"$0
		}
		END{
			print ""
			while ((getline < artfile) >0) {
				print $0
			}
			close (artfile)
		}
	' > ${article}.new
	if [ -s ${article}.new ]; then
		mv ${article} ${article}.bak
		mv ${article}.new $article
		echo "${article}: ok"
	else
		echo "${article}: FAILED"
		rm -f ${article}.new
	fi
	shift
done
