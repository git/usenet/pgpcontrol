# Makefile for pgpcontrol to handle various maintenance functions.

VERSION		= 2.6

# Path to the PGPKEYS and README.html files.  This currently will only work
# for me.  Bleh.
CONTROL		= $$HOME/dvl/control

# Black magic to get the version and modification date arguments for pod2man
# out of the comments in pgpverify and signcontrol.
EXTRACT		= grep '^\# Version'
REFORMAT	= sed -e 's/^\# Version /--release=/' -e 's/, / --date=/'

# Run the test suite.
check test:
	cd tests && ./run-tests

# Generate a staging area to be copied to the FTP site.  This contains the
# documentation and scripts separate, the .tar.gz file for the entire
# distribution in a subdirectory, and the ARCHIVE area.
dist: release pgpverify.8
	mkdir -p staging
	rsync -a ARCHIVE FORMAT pgpverify pgpverify.8 \
	    sample.control sign.sh signcontrol staging/
	cp -p pgpcontrol-$(VERSION).tar.gz staging/
	cp $(CONTROL)/PGPKEYS staging/
	cp $(CONTROL)/README.html staging/
	lynx --dump --nolist staging/README.html > staging/README

# Generate a tarball of my entire working area, but without the archive of old
# versions of the software.
release: pgpverify.8
	rm -f pgpcontrol-$(VERSION).tar.gz
	mkdir -p pgpcontrol-$(VERSION)
	rsync -C -a --exclude /ARCHIVE --exclude /pgpcontrol-* \
	    --exclude /staging ./ pgpcontrol-$(VERSION)/
	tar cfz pgpcontrol-$(VERSION).tar.gz pgpcontrol-$(VERSION)
	rm -rf pgpcontrol-$(VERSION)

# Generate a pgpverify manual page using pod2man.
pgpverify.8: pgpverify
	pod2man `$(EXTRACT) pgpverify | $(REFORMAT)` \
	    --section=8 --center="Maintenance Commands" pgpverify > $@
